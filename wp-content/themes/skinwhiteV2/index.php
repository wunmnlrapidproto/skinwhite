<?php get_header(); ?>

    <div class="home">
        <div style="padding: 0" class="container-fluid">
            <?php include_once('functions/get_carousel.php'); ?>
        </div>
        <div class="container-fluid">
            <div class="row teens">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 product-line"><img src="<?php bloginfo('template_directory');?>/images/home-teens.jpg" class="hero-product hidden-xs"><img src="<?php bloginfo('template_directory');?>/images/fragment-teens.jpg" class="fragment">
                            <h3 class="visible-xs-block">TEENS</h3>
                            <p class="hidden-xs">Start feeling bright with SkinWhite Teens' Vitanourish Formula + Fruity Scent.</p><span class="icon"><div class="icon-right-open"></div></span></div>
                    </div>
                </div>
            </div>
            <div class="row classic">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 product-line"><img src="<?php bloginfo('template_directory');?>/images/home-classic.jpg" class="hero-product hidden-xs"><img src="<?php bloginfo('template_directory');?>/images/fragment-classic.jpg" class="fragment">
                            <h3 class="visible-xs-block">CLASSIC</h3>
                            <p class="hidden-xs">Always feel light &amp; confident with SkinWhite Classic's Vitanourish Formula and Lightfeel Technology.</p><span class="icon"><div class="icon-right-open"></div></span></div>
                    </div>
                </div>
            </div>
            <div class="row advanced">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 product-line"><img src="<?php bloginfo('template_directory');?>/images/home-advanced.jpg" class="hero-product hidden-xs"><img src="<?php bloginfo('template_directory');?>/images/fragment-advanced.jpg" class="fragment">
                            <h3 class="visible-xs-block">ADVANCED</h3>
                            <p class="hidden-xs">Be the brightest you can be in an instant with the Powerwhitening Technology of SkinWhite PowerWhitening.</p><span class="icon"><div class="icon-right-open"></div></span></div>
                    </div>
                </div>
            </div>
            <div class="row naturals">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 product-line"><img src="<?php bloginfo('template_directory');?>/images/home-naturals.jpg" class="hero-product hidden-xs"><img src="<?php bloginfo('template_directory');?>/images/fragment-naturals.jpg" class="fragment">
                            <h3 class="visible-xs-block">NATURALS</h3>
                            <p class="hidden-xs">Look bright &amp; naturally ageless with SkinWhite Naturals' Papaya + Milk.</p><span class="icon"><div class="icon-right-open"></div></span></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div style="background-color: #f1f1f1" class="container-fluid">
        <div class="container">
            <div class="row bright-movements">
                <div class="col-xs-12">
                    <h2>BRIGHT MOVEMENTS</h2></div>
                <div class="col-sm-4 col-md-4"><img src="<?php bloginfo('template_directory');?>/images/line-classic.jpg" class="img-responsive"></div>
                <div class="col-sm-8 col-md-8">
                    <div class="content">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis deserunt, corporis quis modi ipsum, ipsam, quos dignissimos dolorem at, ad quisquam fugiat in asperiores minima nostrum! Earum, obcaecati, fuga. Cumque aspernatur dolorem, perspiciatis est quod, similique facere, rerum repellendus eius qui repudiandae dolor. Maiores ab dicta totam magni corrupti corporis.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fuga maxime eum numquam vero suscipit, officiis repudiandae, magnam alias dolorem dolores at sint quia illum. Officia nulla iste unde, nam itaque earum odit maxime natus dicta maiores deleniti est assumenda, fuga placeat, ea eaque, cum? Ipsa quod nemo iure, cupiditate facere.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
<?php get_footer();?>