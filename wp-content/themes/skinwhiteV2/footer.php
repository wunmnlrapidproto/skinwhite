<div class="container-fluid footer">
      <div class="container">
        <div class="row">
          <div class="col-sm-4 col-md-3 text-center social-links"><a href="#"><span class="icon-facebook-rect"></span></a><a href="#"><span class="icon-instagram-filled"></span></a><a href="#"><span class="icon-twitter-bird"></span></a>
            <hr style="margin: 15% 30% 0" class="visible-xs-block">
          </div>
          <div class="col-md-4 text-center visible-md-block visible-lg-block email-sign-up">
            <h5>SIGN UP FOR OUR NEWSLETTER</h5>
            <form class="form-inline">
              <div class="form-group">
                <label for="email" class="sr-only">Email address</label>
                <input type="email" id="email" placeholder="enter your email" class="form-control">
                <button type="submit" class="btn btn-blue">SEND</button>
              </div>
            </form>
          </div>
          <div class="col-sm-8 col-md-5 address">
            <h5>CONTACT US</h5>
            <address class="small">
              <strong>Splash Corporation</strong><br/>
              5F W Office Bldg., 11th Avenue cor 28th Street,
              Bonifacio Global City, Taguig City 1634 Philippines
              Call us at +632 491 7707
              Send us an email at <a href='#'>info@skinwhite.com.ph</a>
            </address>
          </div>
        </div>
      </div>
    </div>
    

    <script src="<?php bloginfo('template_directory');?>/lib/jquery/dist/jquery.min.js"></script>
    <script src="<?php bloginfo('template_directory');?>/lib/jquery-mousewheel/jquery.mousewheel.min.js"></script>
    <script src="<?php bloginfo('template_directory');?>/lib/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?php bloginfo('template_directory');?>/lib/slick-carousel/slick/slick.min.js"></script>
    <script src="<?php bloginfo('template_directory');?>/lib/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.js"></script>
    <script src="<?php bloginfo('template_directory');?>/scripts/app.js"></script>  
    <?php wp_footer(); //Crucial footer hook! ?>

  </body>
</html>