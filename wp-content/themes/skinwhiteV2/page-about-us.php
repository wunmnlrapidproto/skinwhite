<?php get_header(); ?>

<?php if ( have_posts() ) : ?>

    <?php while ( have_posts() ) : the_post(); ?>

	<div class="about-us">
      <div class="container-fluid">
        <div style="position: relative" class="row">
          <div class="col-md-6 hidden-xs hidden-sm banner" style="background-image:url('<?php echo get_field('main_image_about'); ?>');">
          	
            <div class="inner"></div>
          </div>
          <div class="container">
            <div class="row">
              <div class="col-md-6 col-md-offset-6">
                <div class="content">
                  <h1>ABOUT US</h1>
                  <hr>
                  <p><?php the_content(); ?> </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <?php endwhile; /* rewind or continue if all posts have been fetched */ ?>

<?php else : ?>

    echo '<p>No content found!</p>';


<?php endif; ?>

<?php get_footer(); ?>
