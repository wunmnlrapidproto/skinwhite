$(document).ready(function() {
    $('.items .slider').slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        prevArrow: 'span.prev',
        nextArrow: 'span.next',
        responsive: [{

            breakpoint: 1024,
            settings: {
                slidesToShow: 4
            }

        }, {

            breakpoint: 800,
            settings: {
                slidesToShow: 3
            }

        }, {

            breakpoint: 600,
            settings: {
                slidesToShow: 2
            }

        }, {

            breakpoint: 480,
            settings: {
                slidesToShow: 1
            }

        }]
    });

    $('.scroll-content').mCustomScrollbar({
        'theme': 'minimal-dark'
    });

    $('#itemDetailsModal').on('show.bs.modal', function(event) {

        var button = $(event.relatedTarget); // Button that triggered the modal
        var recipient = button.data('whatever'); // Extract info from data-* attributes

        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.

        var modal = $(this);
        modal.find('.modal-title').text('New message to ' + recipient);
        modal.find('.modal-body input').val(recipient);
    })


    /* Skincare Links */

    $('div.filters a').click(function(e) {

        $('div.filters a').removeClass('active');

        $(this).addClass('active');

    });



});
