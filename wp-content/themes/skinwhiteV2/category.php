<?php get_header(); ?>

<div class="skincare">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="filters">
	                    <a href="<?php echo site_url(); ?>/category/skincare">SKINCARE</a>
	                    <a href="<?php echo site_url(); ?>/category/lifestyle-feature">LIFESTYLE FEATURE</a>
	                    <a href="<?php echo site_url(); ?>/category/through-the-lens">THROUGH THE LENS</a>
	                </div>
                </div>
            </div>
            <div class="row articles">
                <?php include_once("functions/get_category_items.php"); ?>
            </div>
        </div>
    </div>

<?php get_footer(); ?>