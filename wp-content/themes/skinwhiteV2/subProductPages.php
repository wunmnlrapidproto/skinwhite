<?php /* Template Name: Sub Product Page */ ?>

<?php

    // Globally declare variables for Sub Product Page
    $parentPageID = $post->post_parent;
    $curPageID = $post->ID;

?>


<?php get_header(); ?>

<?php if ( have_posts() ) : ?>

    <?php while ( have_posts() ) : the_post(); ?>

    <div class="product classic">
      <div class="jumbotron hero"><img src="<?php echo get_field('main_image'); ?>" class="img-responsive"></div>
      <div class="container">
        <div class="row items">
          <div style="position: relative" class="col-sm-12">
            <div class="slider">
            <?php include_once("functions/fetch_items.php"); ?>
            </div><span class="icon-left-open prev"></span><span class="icon-right-open next"></span>
          </div>
        </div>
        <?php include_once("functions/modal-item.php"); ?>
        <?php if($post->post_name == "classic") : ?>
            <div class="container-fluid">
              <div class="container">
                <div class="row bright-movements" style="margin-top:0 !important;">
                  <div class="col-xs-12">
                  </div>
                  <div class="col-sm-4 col-md-4"><img src="<?php bloginfo('template_directory');?>/images/line-classic.jpg" class="img-responsive"></div>
                  <div class="col-sm-8 col-md-8">
                    <div class="content" style="border-left:none !important;">
                      <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis
                        deserunt, corporis quis modi ipsum, ipsam, quos dignissimos dolorem at,
                        ad quisquam fugiat in asperiores minima nostrum! Earum, obcaecati, fuga.
                        Cumque aspernatur dolorem, perspiciatis est quod, similique facere,
                        rerum repellendus eius qui repudiandae dolor. Maiores ab dicta totam
                        magni corrupti corporis.
                      </p>
                      <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fuga maxime
                        eum numquam vero suscipit, officiis repudiandae, magnam alias dolorem
                        dolores at sint quia illum. Officia nulla iste unde, nam itaque earum
                        odit maxime natus dicta maiores deleniti est assumenda, fuga placeat,
                        ea eaque, cum? Ipsa quod nemo iure, cupiditate facere.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        <?php endif; ?>
        <div class="row features">
          <h2 class="text-center">FEATURE</h2>
          <?php include_once("functions/featured_product.php"); ?>
        </div>
        <div class="row product-lines">
          <div class="col-sm-12">
            <h2 class="text-center">PRODUCT LINE</h2>
          </div>
          <?php include_once("functions/product_line.php"); ?>
        </div>
      </div>
    </div>
    <div style="background-color: #f1f1f1" class="container-fluid">
      <div class="container">
        <div class="row bright-movements">
          <div class="col-xs-12">
            <h2>BRIGHT MOVEMENTS</h2>
          </div>
          <div class="col-sm-4 col-md-4"><img src="<?php bloginfo('template_directory');?>/images/line-classic.jpg" class="img-responsive"></div>
          <div class="col-sm-8 col-md-8">
            <div class="content">
              <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis
                deserunt, corporis quis modi ipsum, ipsam, quos dignissimos dolorem at,
                ad quisquam fugiat in asperiores minima nostrum! Earum, obcaecati, fuga.
                Cumque aspernatur dolorem, perspiciatis est quod, similique facere,
                rerum repellendus eius qui repudiandae dolor. Maiores ab dicta totam
                magni corrupti corporis.
              </p>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fuga maxime
                eum numquam vero suscipit, officiis repudiandae, magnam alias dolorem
                dolores at sint quia illum. Officia nulla iste unde, nam itaque earum
                odit maxime natus dicta maiores deleniti est assumenda, fuga placeat,
                ea eaque, cum? Ipsa quod nemo iure, cupiditate facere.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>

    <?php endwhile; /* rewind or continue if all posts have been fetched */ ?>

<?php else : ?>

    echo '<p>No content found!</p>';


<?php endif; ?>

<?php get_footer(); ?>
