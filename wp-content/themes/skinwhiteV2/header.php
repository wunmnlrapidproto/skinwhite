<!DOCTYPE html>
<html lang="en">
  <head>
    <title>S K I N W H I T E</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="stylesheet" href="<?php bloginfo('template_directory');?>/lib/slick-carousel/slick/slick.css">
    <link rel="stylesheet" href="<?php bloginfo('template_directory');?>/lib/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" href="<?php bloginfo('template_directory');?>/styles/main.css">
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro' rel='stylesheet' type='text/css'>
  </head>
  <body>
  <nav class="navbar navbar-default">
      <div class="container">
        <div class="navbar-header"><a href="#" class="logo"><img src="<?php bloginfo('template_directory');?>/images/logo.png"></a>
          <button type="button" data-toggle="collapse" data-target="#menu" aria-expanded="false" class="navbar-toggle collapsed"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
        </div>
        <div id="menu" class="collapse navbar-collapse">
          <div class="row">
            <div class="col-md-5">
              <ul class="nav navbar-nav navbar-right">
                <li class="dropdown"><a data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle">Products</a>
                  <ul class="dropdown-menu">
                    <li class="nav-classic"><a href="product/classic/">Classic</a></li>
                    <li class="nav-advance"><a href="product/advanced/">Advanced</a></li>
                    <li class="nav-naturals"><a href="product/naturals/">Naturals</a></li>
                    <li class="nav-teens"><a href="product/teens/">Teens</a></li>
                  </ul>
                </li>
                <li><a href="http://mysite.local:88/wordpress-4.5/wordpress/category/skincare/">Skincare</a></li>
              </ul>
            </div>
            <div class="col-md-5 col-md-offset-2">
              <ul class="nav navbar-nav navbar-left">
                <li><a href="<?php bloginfo('template_directory');?>/about-us">About Us</a></li>
                <li><a href="<?php bloginfo('template_directory');?>/contact">Contact Us</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </nav>