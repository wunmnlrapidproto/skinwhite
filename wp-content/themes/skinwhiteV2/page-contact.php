<?php get_header(); ?>

<?php if ( have_posts() ) : ?>

    <?php while ( have_posts() ) : the_post(); ?>

	<div class="contact-us">
      <div class="container-fluid">
        <div style="position: relative" class="row">
          <div class="col-sm-6 hidden-xs map" style="background-image:url('<?php echo get_field('map_image'); ?>');">
            <div class="inner"></div>
          </div>
          <div class="container">
            <div class="row">
              <div class="col-sm-6 col-sm-offset-6">
                <div class="content">
                  <h1>CONTACT US</h1>
                  <hr>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eum veritatis labore ipsam officiis nemo, quo doloribus harum qui quis, quasi, magnam illum exercitationem molestias dignissimos. Dolores ad similique quod vel magni officiis, nam voluptate. Id quisquam, animi dolorum sunt eaque nulla adipisci similique itaque quia, quae incidunt illo, dignissimos consequuntur?</p>
                  <p><?php the_content(); ?> </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>





    <?php endwhile; /* rewind or continue if all posts have been fetched */ ?>

<?php else : ?>

    echo '<p>No content found!</p>';


<?php endif; ?>

<?php get_footer(); ?>
