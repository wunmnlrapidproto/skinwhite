<?php get_header(); 

if (have_posts()):

    while(have_posts()): the_post();

    $image = get_field('main_image');

    $cats    = get_the_category($post->ID);
    $catname = $cats[0]->name;

?>

	<div class="skincare">
        <div class="container">
            <div class="row">
                <div class="post">
                    <div class="col-md-6 feature-img"><img src="<?php echo $image;?>" class="img-responsive"></div>
                    <div class="col-md-6"><img src="<?php bloginfo('template_directory');?>/images/sizer.png" style="width: 100%" class="hidden-xs hidden-sm">
                        <div class="scroll-content">
                            <div class="content">
                               <?php echo the_content(); ?>
                                <div class="share-links"><a href="#"><span class="icon-facebook-rect"></span></a><a href="#"><span class="icon-twitter-bird"></span></a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row articles">
           	 	<?php include_once("functions/get_category_sub_items.php"); ?>
        	</div>
        </div>
    </div>





<?php
    endwhile;
endif;
?>



<?php get_footer(); ?>