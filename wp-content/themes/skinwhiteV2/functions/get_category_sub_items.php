<?php

  $skincare_sub_items_args = array(
        'posts_per_page' => 3,
        'category_name' => $catname,
        'post__not_in' => array($post->ID)
    );


    $skincare_subitems = new WP_Query($skincare_sub_items_args);



if ($skincare_subitems->have_posts()):

    while($skincare_subitems->have_posts()): $skincare_subitems->the_post();


    $image = get_field('thumbnail_image');

?>
    
    <div class="col-sm-6 col-md-4">
        <div class="article">
            <a href="<?php echo esc_url( get_permalink($page->ID) ); ?>"><img src="<?php echo $image;?>" class="img-responsive">
                <div class="blurb"><span class="title"><?php echo the_title();?></span>
                    <hr>
                    <p><?php echo the_field('blurb');?></p>
                </div>
            </a>
        </div>
    </div>



<?php
    endwhile;
endif;
?>
