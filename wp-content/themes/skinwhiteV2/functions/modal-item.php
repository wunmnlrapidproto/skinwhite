<?php
    

        if ($itemQuery->have_posts()):

        while($itemQuery->have_posts()): $itemQuery->the_post();


        $image = get_field('main_image');
        $size = 'full'; // (thumbnail, medium, large, full or custom size)


        // echo '<pre>';
        // print_r($itemQuery);
        // echo '</pre>';

        
    ?>

        <div id="item-<?php echo the_ID();?>" tabindex="-1" role="dialog" class="modal fade itemDetails">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="row">
                            <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">&times;</span></button>
                            <div class="col-md-6"><img src="<?php echo $image;?>"" class="img-responsive"></div>
                            <div class="col-md-6"><img src="<?php bloginfo('template_directory');?>/images/sizer.png" style="width: 100%" class="hidden-xs hidden-sm">
                                <div class="scroll-content">
                                    <div class="content">
                                        <?php echo the_content();?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>



    <?php
        endwhile;
    endif;



?>




