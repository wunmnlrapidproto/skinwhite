<?php

  $cats    = get_the_category( $post->ID ); 
  $catname = $cats[0]->name; 

  $skincare_args = array(
        'category_name' => $catname
   );
   $args = new WP_Query($skincare_args);


if ($args->have_posts()):

    while($args->have_posts()): $args->the_post();

    $image = get_field('main_image');


?>
    
   <div class="col-sm-6 col-md-4">
        <div class="article">
            <a href="<?php echo esc_url( get_permalink($args->ID) ); ?>">
            	<img src="<?php echo $image;?>" class="img-responsive">
                <div class="blurb">
                	<span class="title"><?php echo strtoupper(the_title());?></span>
                    <hr>
                    <p><?php echo the_field('blurb');?></p>
                </div>
            </a>
        </div>
    </div>



<?php
    endwhile;
endif;
?>
