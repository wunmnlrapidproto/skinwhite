<?php

    $post_slug=$post->post_name;

    $itemsArgs = array(
        'posts_per_page' => 5,
        'category_name' => $post_slug
    );


    $itemQuery = new WP_Query($itemsArgs);

    // echo '<pre>';
    // print_r($itemQuery);
    // echo '</pre>';



    if ($itemQuery->have_posts()):

    while($itemQuery->have_posts()): $itemQuery->the_post();


    $image = get_field('thumbnail_image');
    $size = 'thumbnail'; // (thumbnail, medium, large, full or custom size)

    
?>

    <div class="thumbnail">
        <img src="<?php echo $image; ?>" data-toggle="modal" data-target="#item-<?php echo  the_ID();?>">
        <div class="caption text-center">
          <h5><?php echo the_title(); ?><small><br/><?php echo the_field('blurb'); ?></small></h5>
          <a href="<?php echo the_field('external_link'); ?>" target="_blank" class="btn btn-blue">BUY</a>
        </div>
    </div>



<?php
    endwhile;
endif;



?>
