<?php
  $featured_args = array(
        'posts_per_page' => 4,
        'meta_key' => 'featured_post',
        'category_name' => 'skincare',
        'meta_value' => 1
    );
    $featured = new WP_Query($featured_args);


if ($featured->have_posts()):

    while($featured->have_posts()): $featured->the_post();


    $image = get_field('thumbnail_image');
    $size = array(200,200); // (thumbnail, medium, large, full or custom size)

    //echo wp_get_attachment_image( $image, $size );

?>
    
    <div class="col-md-3 col-sm-6">
        <div class="thumbnail"><img src="<?php echo $image;?>">
          <div class="caption text-center">
            <h5><?php echo the_title(); ?></h5>
            <p><?php echo the_field('blurb'); ?></p>
          </div>
        </div>
    </div>



<?php
    endwhile;
endif;
?>
