<?php

function skinwhite_resources() {

    wp_enqueue_style('style', get_stylesheet_uri());

}


add_action('wp_enqueue_scripts','skinwhite_resources');

// Enabling support for Post Thumbnails
//add_theme_support( 'post-thumbnails' );


/* When adding excerpt in Page screen */
// function my_add_excerpts_to_pages() {
//      add_post_type_support( 'page', 'excerpt' );
// }

//add_action( 'init', 'my_add_excerpts_to_pages' );





/**
 * Register our sidebars and widgetized areas.
 *
 */
function rotator_widgets_init() {

    register_sidebar( array(
        'name'          => 'Home Rotator Banner',
        'id'            => 'home_top',
        'before_widget' => '<div>',
        'after_widget'  => '</div>',
        'before_title'  => '<h2 class="rounded">',
        'after_title'   => '</h2>',
    ) );

}
add_action( 'widgets_init', 'rotator_widgets_init' );


?>
