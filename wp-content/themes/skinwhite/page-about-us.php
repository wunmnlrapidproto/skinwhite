<?php get_header(); ?>
<div class="content">
<?php if ( have_posts() ) : ?>

    <?php while ( have_posts() ) : the_post();

        $imageAbout = get_field('main_image_about');
        $size = 'full'; // (thumbnail, medium, large, full or custom size)

    ?>

        <article class="post-page">

                <?php the_content(); ?>
                <?php echo wp_get_attachment_image( $imageAbout, $size ); ?>
        </article>

    <?php endwhile; /* rewind or continue if all posts have been fetched */ ?>

<?php else : ?>

    echo '<p>No content found!</p>';


<?php endif; ?>

</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
