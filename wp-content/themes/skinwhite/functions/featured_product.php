<?php
  $featured_args = array(
        'posts_per_page' => 4,
        'meta_key' => 'featured_post',
        'category_name' => 'skinwhite',
        'meta_value' => 1
    );
    $featured = new WP_Query($featured_args);


    //echo '<pre>';
    //    print_r($featured);
    //echo '</pre>';






if ($featured->have_posts()):

    while($featured->have_posts()): $featured->the_post();


    $image = get_field('thumbnail_image');
    $size = array(200,200); // (thumbnail, medium, large, full or custom size)

    echo wp_get_attachment_image( $image, $size );

?>

    <h4><?php echo the_title(); ?></h4>
    <div><?php echo the_field('blurb'); ?></div>

<?php
    endwhile;
endif;
?>
