
<?php

    /* This will query different Product Pages */


    $productLine_args = array(
    'sort_order' => 'asc',
    'sort_column' => 'post_title',
    'child_of' => $parentPageID,
    'parent' => -1,
    'exclude_tree' => $curPageID,
    'offset' => 0,
    'post_type' => 'page',
    'post_status' => 'publish'

);
    $productLine = get_pages($productLine_args);

    // echo '<pre>';
    // print_r($productLine);
    // echo '</pre>';

    foreach ( $productLine as $page ) {

        $image = get_field('thumbnail_image', $page->ID);
        $size = array(250,250); // (thumbnail, medium, large, full or custom size)

        echo wp_get_attachment_image( $image, $size );


    }


?>


