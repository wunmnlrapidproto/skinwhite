<?php

    $post_slug=$post->post_name;

    $itemsArgs = array(
        'posts_per_page' => 5,
        'category_name' => $post_slug
    );


    $itemQuery = new WP_Query($itemsArgs);

    // echo '<pre>';
    // print_r($itemQuery);
    // echo '</pre>';



    if ($itemQuery->have_posts()):

    while($itemQuery->have_posts()): $itemQuery->the_post();


    $image = get_field('thumbnail_image');
    $size = 'thumbnail'; // (thumbnail, medium, large, full or custom size)

    echo wp_get_attachment_image( $image, $size );

?>

    <h4><?php echo the_title(); ?></h4>
    <div><?php echo the_field('blurb'); ?></div>
    <a href="<?php echo the_field('external_link'); ?>" target="_blank">Buy</a>
    <br />
<?php
    endwhile;
endif;



?>
