<?php /* Template Name: Sub Product Page */ ?>

<?php

    // Globally declare variables for Sub Product Page

    $parentPageID = $post->post_parent;
    $curPageID = $post->ID;
?>


<?php get_header(); ?>
<div class="content">


    <h3>
        Items
    </h3>
    <div>
        <?php include_once("functions/fetch_items.php"); ?>
    </div>

    <h3>
        Featured Product
    </h3>
    <div>
        <?php include_once("functions/featured_product.php"); ?>
    </div>

    <h3>Product Line</h3>
    <div>
        <?php include_once("functions/product_line.php"); ?>
    </div>

</div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>
