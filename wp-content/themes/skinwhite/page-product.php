<?php echo 'Test '. get_page_template_slug( $post->ID ); ?>


<?php get_header(); ?>
<div class="content">
<?php if ( have_posts() ) : ?>

    <?php while ( have_posts() ) : the_post(); ?>

        <article class="post-page">
            <h2>This is page product: <?php the_title(); ?></h2>
                <?php the_excerpt(); ?>
                <br />
                <?php the_content(); ?>
        </article>

    <?php endwhile; /* rewind or continue if all posts have been fetched */ ?>

<?php else : ?>

    echo '<p>No content found!</p>';


<?php endif; ?>

</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
