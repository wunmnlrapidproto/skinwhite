<?php get_header(); ?>
<div class="content">
<?php if ( have_posts() ) : ?>

    <?php while ( have_posts() ) : the_post(); ?>

        <article class="post-page">
            <h2><?php the_title(); ?></h2>
        </article>
        <p>
           Featured: <?php the_field('featured_post'); ?>
        </p>
        <p>
           The blurb: <?php the_field('blurb'); ?>
        </p>

        <p>
           URL link: <?php the_field('buy_link'); ?>
        </p>

        <p>
           Featured Image: <?php the_field('featured_image'); ?>
        </p>

        <p>
           Main Image: <?php the_field('thumbnail_image'); ?>
        </p>


    <?php endwhile; /* rewind or continue if all posts have been fetched */ ?>

<?php else : ?>

    echo '<p>No content found!</p>';


<?php endif; ?>

</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
