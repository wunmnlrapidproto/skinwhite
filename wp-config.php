<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'skinwhite_db');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', '');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '|-yBFlx:df_^vg/5YW]R lh,?;Ca2Ars-BG$V]6k s aE-4~eYGpNhn%j(h5_2gg');
define('SECURE_AUTH_KEY',  'F`OTw,E 22y_T<h,jlQn0m0q$z4Aqvq*7z[[T;m}ny3`C<9O+uzdo>2Hv!C@^k=Y');
define('LOGGED_IN_KEY',    '/rhg9|M0MHGc2a;8z$[[MV9tJ^4Ldp>z5eC1HZaQqk=-[}$B.(Y=`Mi%mXoZ(>/c');
define('NONCE_KEY',        'Rq>U=G[>>XSr-c6N7~+h)W0FXP4~#Jh!ehy*HFR$&oPlS)aAW$F!QsJdUbytNOsR');
define('AUTH_SALT',        'K8F,iqRilq9L,MV)79_P7aoQ?&mDazymy}Nw/<Yqa;cZ%BUs*B?S2dje0NP`H&ir');
define('SECURE_AUTH_SALT', '8:N.%`{Ju*f6hjjS4U$W%y[pTV4M,APwIgq+LwNc5}P/Uo|#`/*(|X_G@H,g{8c3');
define('LOGGED_IN_SALT',   '%X|5~}`(925!1Xi~j8ta}M/WZEowtgZ^p]Yh?fi+ha,W}gxeavwSle4mC^*,SuX!');
define('NONCE_SALT',       'a0F$bCZ.p>W!me{$Oa<~)~_Y=MI?adC9!F+cn}tGqWo9=m%RN.a4/1Xx`>y}1Q@.');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'skinwhite_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */


/* Added Configuration for Multisite Installtion of Wordpress */  
define( 'WP_ALLOW_MULTISITE', true );




/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
